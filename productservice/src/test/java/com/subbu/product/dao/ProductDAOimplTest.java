package com.subbu.product.dao;

import static org.junit.Assert.*;

import org.junit.Test;

import com.subbu.product.dto.Product;

public class ProductDAOimplTest {

	@Test
	public void createShouldCreateProject() {
		ProductDAO dao=new ProductDAOimpl();
		Product product = new Product();
		product.setId(1);
		product.setName("IPhone");
		product.setDescription("it is awesome");
		product.setPrice(10000);
		
		Product result=dao.read(1);
		assertNotNull(result);
		assertEquals("IPhone", result.getName());
		
	}

}
